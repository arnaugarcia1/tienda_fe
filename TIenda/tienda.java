package Tienda;

public class tienda {

    Arma espada = new Arma("espada", 1000, 12, 10);
    Arma lanza = new Arma("lanza", 1200, 10, 7);
    Arma hacha = new Arma("hacha", 850, 14, 12);
    Arma puños = new Arma("caestus", 650, 7, 10);

    Bastones curar = new Bastones("curar", 2000, 1, 14, 15);
    Bastones mend = new Bastones("reparar", 3000, 1, 25, 10);
    Bastones sanar = new Bastones("sanar", 3500, 8, 12, 8);
    Bastones fortalecer = new Bastones("fortalecer", 5000, 12, 99, 2);

    public void armería() {
        System.out.println("$$----- Armería -----$$");
        System.out.println("Nombre -- precio -- daño -- Cantidad");
        System.out.println(espada);
        System.out.println(lanza);
        System.out.println(hacha);
        System.out.println(puños);
    }

    public void bastones() {
        System.out.println("$$----- Tienda de bastones -----$$");
        System.out.println("Nombre -- precio -- rango -- cura -- Cantidad");
        System.out.println(curar);
        System.out.println(mend);
        System.out.println(sanar);
        System.out.println(fortalecer);
    }
}